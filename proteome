# Project "English words in human proteome"
# Elise André
# Master EFCE - 2019

##############################################################################################

# Fonction readFastaFile2
# Aim : allows to read Fasta file
# Parameter : filename (name of the file)
# Output : s (string with human proteome)    

def readFastaFile2(filename) :
    
    # opening the file whose name is filename
    fd = open(filename,'r')
    txt = fd.read()
    fd.close()
    
    # txt contains all the text of the file. 
    # first, I want to seperate the proteins, the symbol that starts a new protein is '>'
    seqs = txt.split('>')[1:]
    s = ""
    
    for seq in seqs :
        lines = seq.split('\n')[1:]       
        for line in lines :
            s = s + line
    
    return(s)

#fasta_file = readFastaFile2("C:/Users/elise/Desktop/Cours ACO/M2/ICE. Introduction to Computational Ecology/human-proteome.fasta")
fasta_file = readFastaFile2("C:/Users/elise/Desktop/Cours ACO/M2/ICE. Introduction to Computational Ecology/human-proteome-small.fasta")
print(fasta_file)

##############################################################################################

# To read the text file with the English words
text_file = open("C:/Users/elise/Desktop/Cours ACO/M2/ICE. Introduction to Computational Ecology/english-words.txt")
words = text_file.readlines() # Allows to associate one ligne with one word
print(words)

##############################################################################################

# Fonction finding_word
# Aim : allows to find the occurrences of the words in the human proteome
# Parameters : - dictionnary (all the English words)
#              - fasta (Fasta file with human proteome)
# Output : list_number (list with the words and their occurrence)

def finding_word (dictionnary, fasta) :
    length_fasta = len(fasta)            # To know the length of the proteome
    fasta = fasta.lower()                # To put all the letters in small
    list_number = {}                     # An empty list
    
    for one_word in dictionnary :
        one_word = one_word.lower()      # To put all the letters in small
        one_word = one_word[:-1]         # To remove the '\n' at the end of the words
        length_word = len(one_word)      # To know the length of each word 
        
        beg_word = 0                     # Beginning of the word (here 0)
        end_word = length_word           # End of the word (here the length)
    
        number = 0                       # Number is to count occurrences
        
        while end_word <= length_fasta :              # Allows to check if we are not at the end of the fasta file
            letter_fasta = fasta[beg_word:end_word]   # Selection of one letter in the proteome

            if one_word == letter_fasta :             # Checking and adding 1 if it is the same
                number = number + 1
    
            beg_word = beg_word + 1                   # We move a notch
            end_word = end_word + 1

        list_number[one_word] = number                # Creation of the list

    return(list_number)
  

occurrence = finding_word(words,fasta_file)
print(occurrence)

##############################################################################################

# Fonction selected
# Aim : allows to sort the words that do not appear in the human proteome
# Parameters : - occurrence_words (occurrences of the words)
#              - dictionnary (all the English words)
# Output : present_words (list with the words and their occurrence if differs from 0)

def selected (occurrence_words, dictionnary) : 
    present_words = {}          # Creation of a new list
    for i in dictionnary :
        i = i[:-1]              # To remove the '\n' at the end of the words
        i = i.lower()           # To put all the letters in small
        if occurrence_words[i] != 0 :
            present_words[i]=occurrence_words[i]  # We add the word and it occurrence if the word appears in the human proteome
            
    return(present_words)

sorted_words = selected(occurrence, words)

print(sorted_words)

##############################################################################################

# Fonction selected_mini
# Aim : allows to sort the words that appears less than 5 times
# Parameters : - occurrence_words (occurrences of the words)
#              - dictionnary (all the English words)
# Output :     - X (list of the words without the occurrences)
#              - present_words (list with the words and their occurrence if appears less than 100 times)

def selected_mini (occurrence_words, dictionnary) : 
    present_words = {}         # Creation of a new dictionnary
    X = []                     # Creation of a new list
    for i in dictionnary :
        i = i[:-1]             # To remove the '\n' at the end of the words
        i = i.lower()          # To put all the letters in small
        if occurrence_words[i] < 5 and occurrence_words[i] != 0 :
            X.append(i)
            present_words[i]=occurrence_words[i]   # We add the word and it occurrence if the word appears less than 5 times
            
    return(X,present_words)

occurrence_mini = selected_mini(occurrence, words)
print(occurrence_mini)

##############################################################################################

# Fonction selected_inf
# Aim : allows to sort the words that appears less than 100 times
# Parameters : - occurrence_words (occurrences of the words)
#              - dictionnary (all the English words)
# Output :     - X (list of the words without the occurrences)
#              - present_words (list with the words and their occurrence if appears more than 5 times and less than 20 times)

def selected_inf (occurrence_words, dictionnary) : 
    present_words = {}         # Creation of a new dictionnary
    X = []                     # Creation of a new list
    for i in dictionnary :
        i = i[:-1]             # To remove the '\n' at the end of the words
        i = i.lower()          # To put all the letters in small
        if occurrence_words[i] < 20 and occurrence_words[i] > 5 and occurrence_words[i] != 0 :
            X.append(i)
            present_words[i]=occurrence_words[i]   # We add the word and it occurrence if the word appears les than 100 times
            
    return(X,present_words)

occurrence_inf = selected_inf(occurrence, words)
print(occurrence_inf)

##############################################################################################

# Fonction selected_mid
# Aim : allows to sort the words that appears more than 30 and less than 100 times
# Parameters : - occurrence_words (occurrences of the words)
#              - dictionnary (all the English words)
# Output :     - X (list of the words without the occurrences)
#              - present_words (list with the words and their occurrence if appears more than 20 times and less than 100 times)

def selected_mid (occurrence_words, dictionnary) : 
    present_words = {}         # Creation of a new dictionnary
    X = []                     # Creation of a new list
    for i in dictionnary :
        i = i[:-1]             # To remove the '\n' at the end of the words
        i = i.lower()          # To put all the letters in small
        if occurrence_words[i] < 100 and occurrence_words[i] != 0 and occurrence_words[i] > 20  :
            X.append(i)
            present_words[i]=occurrence_words[i]   # We add the word and it occurrence if the word appears les than 100 times
            
    return(X,present_words)

occurrence_mid = selected_mid(occurrence, words)
print(occurrence_mid)

##############################################################################################

# Fonction selected_sup
# Aim : allows to sort the words that appears more than 100 times
# Parameters : - occurrence_words (occurrences of the words)
#              - dictionnary (all the English words)
# Output :     - X (list of the words without the occurrences)
#              - present_words (list with the words and their occurrence if appears more than 100 times)

def selected_sup (occurrence_words, dictionnary) : 
    present_words = {}         # Creation of a new dictionnary
    X = []                     # Creation of a new list
    for i in dictionnary :
        i = i[:-1]             # To remove the '\n' at the end of the words
        i = i.lower()          # To put all the letters in small
        if occurrence_words[i] > 100 and occurrence_words[i] != 0 :
            X.append(i)
            present_words[i]=occurrence_words[i]   # We add the word and it occurrence if the word appears les than 100 times
            
    return(X, present_words)

occurrence_sup = selected_sup(occurrence, words)
print(occurrence_sup,type(occurrence_sup))

##############################################################################################

# Now, we will make some graphs. For that, we import some packages.

import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline

# Graph for mini -> less than 5 times : 
X = occurrence_mini[0]
Y = []
for i in X :
    y = occurrence_mini[1][i]
    Y.append(y)
    
plt.bar(X,Y)
plt.xlabel("Words that appears less than 5 times")
plt.ylabel("Occurrences of the words that appears less than 5 times")
plt.show()

##############################################################################################

# Graph for inf -> more than 5 times and less than 20 times : 
X = occurrence_inf[0]
Y = []
for i in X :
    y = occurrence_inf[1][i]
    Y.append(y)
    
plt.bar(X,Y)
plt.xlabel("Words that appears more than 5 times and less than 20 times")
plt.ylabel("Occurrences of the words that appears more than 5 times and less than 20 times")
plt.show()

##############################################################################################

# Graph for mid -> more than 20 times and less than 100 times : 
X = occurrence_mid[0]
Y = []
for i in X :
    y = occurrence_mid[1][i]
    Y.append(y)
    
plt.bar(X,Y)
plt.xlabel("Words that appears more than 20 times and less than 100 times")
plt.ylabel("Occurrences of the words that appears more than 20 times and less than 100 times")
plt.show()

##############################################################################################

#Graph for sup -> more than 100 times :  
X = occurrence_sup[0]
Y = []
for i in X :
    y = occurrence_sup[1][i]
    Y.append(y)
    
plt.bar(X, Y)
plt.xlabel("Words that appears more than 100 times")
plt.ylabel("Occurrences of the words that appears more than 100 times")
plt.show()

##############################################################################################